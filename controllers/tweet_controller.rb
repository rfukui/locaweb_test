class TweetController
  require_relative '../connection'
  require_relative '../models/tweet'

  def self.most_relevants
    order_by_relevants(Tweet.all_elegibles)
  end

  def self.most_mentions
    group_by_users(Tweet.all_elegibles)
  end

  def self.order_by_relevants(tweets)
    tweets.sort_by { |t| [-1 * t.followers_count.to_i, -1 * t.retweet_count.to_i, -1 * t.favorite_count.to_i] }
  end

  def self.group_by_users(tweets)
    grouped_tweets = {}
    tweets.sort_by { |t| [-1 * t.followers_count.to_i, t.screen_name] }.each do |t|
      grouped_tweets[t.screen_name] = [] unless grouped_tweets[t.screen_name]
      grouped_tweets[t.screen_name] << t
    end
    grouped_tweets.map { |gt| { gt[0] => order_by_relevants(gt[1]).map(&:to_hash) } }
  end
end
