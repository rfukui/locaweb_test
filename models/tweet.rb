class Tweet
  require_relative '../connection'
  attr_accessor :followers_count, :screen_name, :retweet_count, :favorite_count, :text, :created_at, :profile_link, :link, :reply_to, :mentions

  def initialize(followers_count, id, screen_name, retweet_count, favorite_count, text, created_at, profile_link, link, reply_to, mentions)

    @followers_count = followers_count
    @id = id
    @screen_name = screen_name
    @retweet_count = retweet_count
    @favorite_count = favorite_count
    @text = text
    @created_at = created_at
    @profile_link = profile_link
    @link = link
    @reply_to = reply_to
    @mentions = mentions_ids(mentions)
  end

  def elegible?
 
    return true  if ENV['TWITTER_ID'].to_i != self.reply_to and self.mentions.include?(ENV['TWITTER_ID'].to_i)

    false
  end


  def self.all
    tweets = []
    Connection.all_tweets['statuses'].each do |t|
      tweet = Tweet.new(
        followers_count = t['user']['followers_count'],
        id = t['id'],
        screen_name = t['user']['screen_name'],
        retweet_count = t['retweet_count'],
        favorite_count = t['favorite_count'],
        text = t['text'],
        created_at = t['created_at'],
        profile_link = ENV['TWITTER_PROFILE_URL'] + t['user']['screen_name'] ,
        link = ENV['TWITTER_PROFILE_URL'] + t['user']['screen_name'] + '/' + ENV['STATUS'] + '/' + t['id_str'],
        reply_to = t['in_reply_to_user_id'],
        mentions = (t['entities']['user_mentions'])
      )
      tweets<<tweet
    end
    tweets
  end

  def self.all_elegibles
    tweets =[]
    all.each{|t| tweets << t if t.elegible?}
    tweets
  end

  def to_hash
    {
      :followers_count => @followers_count,
      :id => @id,
      :screen_name => @screen_name,
      :retweet_count => @retweet_count,
      :favorite_count => @favorite_count,
      :text => @text,
      :created_at => @created_at,
      :profile_link => @profile_link,
      :link => @link,
      :reply_to => @reply_to,
      :mentions => @mentions,
    }
  end

  private

  def mentions_ids(mentions)
    mentions.map{|m| m['id']}
  end
end
