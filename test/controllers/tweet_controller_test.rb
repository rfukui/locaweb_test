require_relative '../test_helper'

class TweetControlerTest < Test::Unit::TestCase
  @@list_of_tweets = [
    Tweet.new(
      followers_count = 10,
      id = 1,
      screen_name = 'teste',
      retweet_count = 1,
      favorite_count = 1,
      text = 'none',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = 1,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
    ),
    Tweet.new(
      followers_count = 2,
      id = 2,
      screen_name = 'teste2',
      retweet_count = 10,
      favorite_count = 1,
      text = 'none',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = 1,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
    ),
    Tweet.new(
      followers_count = 2,
      id = 3,
      screen_name = 'teste3',
      retweet_count = 1,
      favorite_count = 1,
      text = 'none',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = 1,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
    ),
    Tweet.new(
      followers_count = 10,
      id = 1,
      screen_name = 'teste',
      retweet_count = 10,
      favorite_count = 1,
      text = 'none_2',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = 1,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
    )
  ]
  def test_order_by_relevants
    ordered_tweets = [
      Tweet.new(
        followers_count = 10,
        id = 1,
        screen_name = 'teste',
        retweet_count = 10,
        favorite_count = 1,
        text = 'none_2',
        created_at = 'Mon Sep 24 03:35:21 +0000 2012',
        profile_link = 'https://twiiter.com/teste',
        link = 'https://twiiter.com/teste/status/42',
        reply_to = 1,
        mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
      ),
      Tweet.new(
        followers_count = 10,
        id = 1,
        screen_name = 'teste',
        retweet_count = 1,
        favorite_count = 1,
        text = 'none',
        created_at = 'Mon Sep 24 03:35:21 +0000 2012',
        profile_link = 'https://twiiter.com/teste',
        link = 'https://twiiter.com/teste/status/42',
        reply_to = 1,
        mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
      ),
      Tweet.new(
        followers_count = 2,
        id = 2,
        screen_name = 'teste2',
        retweet_count = 10,
        favorite_count = 1,
        text = 'none',
        created_at = 'Mon Sep 24 03:35:21 +0000 2012',
        profile_link = 'https://twiiter.com/teste',
        link = 'https://twiiter.com/teste/status/42',
        reply_to = 1,
        mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
      ),
      Tweet.new(
        followers_count = 2,
        id = 3,
        screen_name = 'teste3',
        retweet_count = 1,
        favorite_count = 1,
        text = 'none',
        created_at = 'Mon Sep 24 03:35:21 +0000 2012',
        profile_link = 'https://twiiter.com/teste',
        link = 'https://twiiter.com/teste/status/42',
        reply_to = 1,
        mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
      )
    ]
    tweets = TweetController.order_by_relevants(@@list_of_tweets)
    assert_equal tweets.map(&:to_hash), ordered_tweets.map(&:to_hash)
  end

  def test_group_by_users
    grouped_tweets = TweetController.group_by_users(@@list_of_tweets)
    assert_equal grouped_tweets, [{ 'teste' => [{ followers_count: 10, id: 1, screen_name: 'teste', retweet_count: 10, favorite_count: 1, text: 'none_2', created_at: 'Mon Sep 24 03:35:21 +0000 2012', profile_link: 'https://twiiter.com/teste', link: 'https://twiiter.com/teste/status/42', reply_to: 1, mentions: [42, 245_012] }, { followers_count: 10, id: 1, screen_name: 'teste', retweet_count: 1, favorite_count: 1, text: 'none', created_at: 'Mon Sep 24 03:35:21 +0000 2012', profile_link: 'https://twiiter.com/teste', link: 'https://twiiter.com/teste/status/42', reply_to: 1, mentions: [42, 245_012] }] },
                                  { 'teste2' => [{ followers_count: 2, id: 2, screen_name: 'teste2', retweet_count: 10, favorite_count: 1, text: 'none', created_at: 'Mon Sep 24 03:35:21 +0000 2012', profile_link: 'https://twiiter.com/teste', link: 'https://twiiter.com/teste/status/42', reply_to: 1, mentions: [42, 245_012] }] },
                                  { 'teste3' => [{ followers_count: 2, id: 3, screen_name: 'teste3', retweet_count: 1, favorite_count: 1, text: 'none', created_at: 'Mon Sep 24 03:35:21 +0000 2012', profile_link: 'https://twiiter.com/teste', link: 'https://twiiter.com/teste/status/42', reply_to: 1, mentions: [42, 245_012] }] }]
  end
end
