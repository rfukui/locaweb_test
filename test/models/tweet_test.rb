require_relative '../test_helper'

class TweetTest < Test::Unit::TestCase
  def test_elegible
    tweet = Tweet.new(
      followers_count = 0,
      id = 42,
      screen_name = 'teste',
      retweet_count = 1,
      favorite_count = 1,
      text = 'none',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = 1,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => ENV['TWITTER_ID'].to_i, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
    )

    assert_equal true, tweet.elegible?
  end

  def test_not_elegible_by_mention
    tweet = Tweet.new(
      followers_count = 0,
      id = 42,
      screen_name = 'teste',
      retweet_count = 1,
      favorite_count = 1,
      text = 'none',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = 1,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }, { 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 245_012, 'id_str' => '245012' }]
    )
    assert_equal false, tweet.elegible?
  end

  def test_not_elegible_by_reply
    tweet = Tweet.new(
      followers_count = 0,
      id = 42,
      screen_name = 'teste',
      retweet_count = 1,
      favorite_count = 1,
      text = 'none',
      created_at = 'Mon Sep 24 03:35:21 +0000 2012',
      profile_link = 'https://twiiter.com/teste',
      link = 'https://twiiter.com/teste/status/42',
      reply_to = ENV['TWITTER_ID'].to_i,
      mentions = [{ 'screen_name' => 'herman_cale', 'name' => 'Herman Cale', 'id' => 42, 'id_str' => '42' }]
    )
    assert_equal false, tweet.elegible?
  end
end
