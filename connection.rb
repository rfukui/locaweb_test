class Connection
  require 'net/http'
  require 'json'

  def self.all_tweets
    uri = URI(ENV['TWITTER_API'] + '?q=' + ENV['QUERY'])
    req = Net::HTTP::Get.new(uri)
    req['Content-Type'] = 'application/json'
    req['Username'] = ENV['HTTP_USERNAME']
    res = Net::HTTP.start(uri.hostname, uri.port) { |http| http.request(req) }
    JSON.parse(res.body)
  end
end
