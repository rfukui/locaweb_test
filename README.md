# Teste Locaweb v.0.0.1

**Este é um teste escrito como parte do processo de seleção da Locaweb**
**Totalmente escrito em ruby + Framework SINATRA**

## Requerimentos de instalação e execução do programa:
O programa roda em um container tipo Docker, com Dockerfile já pronto
```
$ docker build -t app .
$HTTP_USERNAME='<email de usuario>' docker run -it -p 3000:3000 -e HTTP_USERNAME app
```
Ele criará uma tela simples em http://localhost:3000/
além dos endpoints solicitados

## Executando testes:
Podemos executar de 2 formas
```
$ docker build -t app .
$HTTP_USERNAME='<email de usuario>' docker run -it -p 3000:3000 -e HTTP_USERNAME /bin/bash
#rake
```
ou instalar localmente
```
$bundle install
$rake
```

## Configuração do programa:
`config.rb`
```
o arquivo config.rb possui todas as variaveis, que inclusive podem ser passadas por variaveis de ambiente.

```

```
Foi me solicitado que  programasse em ruby.
A utilização do Sinatra se deve ao fato de ser a biblioteca mais simples para aplicações WEB sem precisar persistir em banco.
foram escritos testes simples devido a natureza da aplicação.
```
