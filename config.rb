class Config
  ENV['HTTP_USERNAME'] ||= 'teste@teste.com'
  ENV['TWITTER_ID'] ||= '42'
  ENV['TWITTER_API'] ||= 'http://tweeps.locaweb.com.br/tweeps'
  ENV['QUERY'] ||= 'locaweb'
  ENV['TWITTER_PROFILE_URL'] ||= 'https://www.twitter.com/'
  ENV['STATUS'] ||= 'status'
end
