require 'sinatra'
require_relative 'config'
require_relative 'connection'
require_relative 'controllers/tweet_controller'
set :bind, '0.0.0.0'
set :public_folder, 'public'

get '/most_relevants' do
  content_type :json
  TweetController.most_relevants.map(&:to_hash).to_json
end

get '/most_mentions' do
  content_type :json
  TweetController.most_mentions.to_json
end

get '/' do
  redirect '/index.html'
end
